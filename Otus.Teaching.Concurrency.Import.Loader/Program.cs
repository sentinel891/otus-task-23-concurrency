﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private CustomerRepository repository { get; set; }

        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            services.AddDbContext<ConcurrencyDbContext>(options =>
            {
                options.UseInMemoryDatabase("Customers");
            });
            services.AddSingleton<CustomerRepository>();

            ServiceProvider serviceProvider = services.BuildServiceProvider();
            CustomerRepository? repository = serviceProvider.GetService<CustomerRepository>();



            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            List<Customer> customers = new List<Customer>();

            customers = GenerateCustomersDataFile();
            //customers = GenerateCustomersDataFile(10, false);
            //customers = GenerateCustomersDataFile(10, true);

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var loader = new FakeDataLoader(repository);
            loader.LoadData(customers);

            Console.WriteLine($"Number of customers in databse: {(int)repository.db.Customers.Local.Count}!");
            
        }

        static List<Customer> GenerateCustomersDataFile(int ThreadNumber = 0, bool UseThreadpool = false)
        {
            Console.WriteLine($"Generator started with process Id {Process.GetCurrentProcess().Id}...");

            string generationType = "";
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000000);
            if (ThreadNumber == 0)
            {
                xmlGenerator.Generate();
                generationType = "No thread";
            }
            else if (UseThreadpool)
            {
                xmlGenerator.GenerateWithThreadpool(ThreadNumber);
                generationType = "Threadpool"; 
            }
            else
            {
                xmlGenerator.GenerateWithThreads(ThreadNumber);
                generationType = "Thread";
            }

            stopWatch.Stop();
            Console.WriteLine($"{generationType} generation time with {ThreadNumber} threads: {stopWatch.ElapsedMilliseconds}");

            XmlParser parser = new XmlParser(_dataFilePath);

            return parser.Parse();
            //return xmlGenerator._customersList.Customers;
        }
    }
}