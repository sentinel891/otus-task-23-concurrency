using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader //: IDataLoader
    {
        private CustomerRepository _repository { get; }

        public FakeDataLoader(CustomerRepository repository)
        {
            _repository = repository;
        }
        public void LoadData(List<Customer> Customers)
        {
            Console.WriteLine("Loading data...");

            foreach (var customer in Customers)
            {
                _repository.Add(customer);
            }

            Console.WriteLine("Loaded data...");
        }
    }
}