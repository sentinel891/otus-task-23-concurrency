using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        Task Add(Customer customer);
    }
}