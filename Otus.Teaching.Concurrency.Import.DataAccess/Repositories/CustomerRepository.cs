using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public readonly ConcurrencyDbContext db;

        public CustomerRepository(ConcurrencyDbContext dbContext)
        {
            this.db = dbContext;
        }

        public Task Add(Customer customer)
        {
            return db.Customers.AddAsync(customer).AsTask();
        }

        public Task ClearAll()
        {
            return Task.Run(() =>
            {
                foreach (var customer in db.Customers.ToListAsync().Result)
                {
                    db.Customers.Remove(customer);
                }
            });
        }
    }
}