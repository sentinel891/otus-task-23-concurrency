﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public XmlParser(string filename)
        {
            _fileName = filename;
        }

        public List<Customer> Parse()
        {
            var customersData = new CustomersList();
            XmlSerializer formatter = new XmlSerializer(typeof(CustomersList));
            using (FileStream fs = new FileStream(_fileName, FileMode.OpenOrCreate))
            {
                customersData = formatter.Deserialize(fs) as CustomersList;
            }
            return customersData.Customers;
        }
    }
}