﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Context
{
    public class ConcurrencyDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public ConcurrencyDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}
